
return {
  name = "Player",
  speed = 300,
  firerate = 10,
  bullet_specname = 'simple-bullet',
  hitbox = { -4, 4, -4, 4 },
  view = {
    color = { .7, .2, .7 },
    type = 'polygon',
    params = { 'fill',  -10, 5, 10, 5, 0, -15 }
  }
}

