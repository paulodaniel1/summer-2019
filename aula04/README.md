
# Aula 04 (17/1/2019) - game it on, part TWO

Nessa aula, nós vamos refazer o exercício da aula anterior seguindo um passo a
passo. O exercício consistem em desenvolver um jogo 2D shooter bem simples, que
pode ser visto e testado na pasta `gabarito` desta aula.

### Índice

[0. Preparativos](#0-preparativos)

[1. O Jogador](#1-o-jogador)

[2. Inimigos](#2-inimigos)

[3. Entidades](#3-entidades)

[4. Continuação](#continuação)

## 0. Preparativos

Antes de começar a programar, precisamos preparar o ambiente de desenvolvimento.

### 0.1 Comece um projeto vazio de LÖVE

Se você já está confortável com a LÖVE pode só fazer o que o título diz e seguir
para o [passo 0.2](#02-ajuste-o-tamanho-da-janela). Se estiver com dúvidas,
continue lendo.

Como vimos na [primeira aula](aula01), a LÖVE é um programa que recebe de
entrada uma pasta com um projeto de jogo (escrito em Lua) e executa ele.
Em particular, ela procura por um arquivo `main.lua`, que tem que definir o
**Game Loop** do jogo. Lembrando que um Game Loop tem mais ou menos o seguinte
formato:

```lua
love.load()
while true do
  love.update(dt)
  love.draw()
end
```

Sendo que nós, desenvolvedores, somo responsáveis por implementar as funções
`love.load()`, `love.update(dt)` e `love.draw()`.

Então crie uma pasta nova no seu computador e em seguida crie um arquivo
`main.lua` dentro dela. Teste para ver se a LÖVE está funcionando normalmente.


### 0.2 Ajuste o tamanho da janela

Como o jogo é um shooter vertical, não queremos uma janela muito larga pois fica difícil de ir de um lado pro outro. Para definir um tamanho de janela diferente do padrão (que é 800x600 pixels), nós precisamos dizer isso no `conf.lua`, que é o outro arquivo especial que a LÖVE procura na pasta do nosso jogo.

Então crie um arquivo `conf.lua` e escreva nele:

```lua
-- conf.lua
function love.conf(t)
  t.window.width = 400
  t.window.height = 600
end
```

Para saber como essas configurações funcionam e quais outras configurações existem, vejam a [documentação oficial](https://love2d.org/wiki/Config_Files).

## 1. O jogador

A primeira coisa que vamos fazer nesse jogo é ter um avatar do jogador que podemos controlar com as teclas direcionais. Nós já fizemos isso no exercício da segunda aula, então sabemos que é preciso guardar o estado do jogador – isto é, sua posição – para poder movimentar ele apropriadamente.


### 1.1 Estado do jogador

Fazemos isso criando uma variável `_player` e guardando nela uma tabela com os dados do estado do jogador:

```lua
-- main.lua
local _player

function love.load()
  -- Cria o jogador na origem (onde é a origem na tela?)
  _player = { x = 0, y = 0 }
end
```

### 1.2 Visual do jogador

Depois, desenhamos o jogador. No caso, ele vai ser um triângulo por simplicidade:

```lua
-- main.lua
function love.draw()
  -- Desenha um triângulo rosa
  love.graphics.setColor( .7, .2, .7 )
  love.graphics.polygon('fill', -10, 5,  -- Canto esquerdo inferior
                                10, 5,   -- Canto direito inferior
                                0, -15)  -- Topo do triângulo
end
```

Isso vai desenhar o jogador no canto esquero superior da tela, que é a origem. Mas queremos que o jogador comece no centro da tela, perto da margem inferior. Então mudamos sua posição inicial com base no tamanho da janela:

```lua
-- main.lua
function love.load()
  -- Pega dimensões da janela
  local W,H = love.graphics.getDimensions()
  -- Cria o jogador no centro em baixo da janela
  _player = { x = W/2, y = 9*H/10 }
end
```

Note que o jogador continua no canto superior esquerdo. Isso ocorre porque nós não usamos sua posição na hora de desenhar! Pra isso, poderíamos fazer:

```lua
-- main.lua
function love.draw()
  local x, y = _player.x, _player.y
  -- Desenha um triângulo rosa na posição do jogador
  love.graphics.setColor( .7, .2, .7 )
  love.graphics.polygon('fill', -10 + x, 5 + y,  -- Canto esquerdo inferior
                                10 + x, 5 + y,   -- Canto direito inferior
                                0 + x, -15 + y)   -- Topo do triângulo
end
```

Um jeito bem mais conveniente é deslocar o sistema de coordenadas inteiro para a posição do jogador:

```lua
-- main.lua
function love.draw()
  -- Desenha um triângulo rosa na posição do jogador
  love.graphics.translate(_player.x, _player.y)
  love.graphics.setColor( .7, .2, .7 )
  love.graphics.polygon('fill', -10, 5,  -- Canto esquerdo inferior
                                10, 5,   -- Canto direito inferior
                                0, -15)  -- Topo do triângulo
end
```

### 1.3 Movimento do jogador

Para mover o jogador, precisamos ver quais teclas direcionais estão apertadas e mudar sua posição de acordo com sua velocidade, usando a fórmula do movimento retilíneo uniforme (também conhecida como a fórmula do sorvete):

```
posição nova = posição antiga + velocidade * variação de tempo
```

No caso, vamos fazer isso em duas dimensões. Em Lua, isso fica:

```lua
-- main.lua
function love.update(dt)
  -- Move o jogador a 300 pixels/segundo... mas em que direção?
  _player.x = _player.x + 300 * dt
  _player.y = _player.y + 300 * dt
end
```

Só que isso faz o jogador só se mover em uma direção, não levando em consideração o controle do jogo. Para levar o controle em consideração, fazemos:


```lua
-- main.lua
function love.update(dt)
  local dirx, diry = 0, 0
  if love.keyboard.isDown('up') then
    diry = diry - 1
  end
  -- ...
  -- Repete para down, left e right
  -- ...
  -- Move o jogador a 300 pixels/segundo
  _player.x = _player.x + dirx * 300 * dt
  _player.y = _player.y + dixy * 300 * dt
end
```

Mas, na vida real, os jogadores podem mudar os controles do jogo. Para isso, podemos fazer uma tabela que mapeia cada botão em uma direção de movimento diferente. O problema, porém, é que uma "direção" é algo que precisa de dois valores, na forma (x, y). Como é chato ter que fazer todas as contas em dobro, nós vamos usar objetos `Vec`, que representam um vetor em R². Esses objetos estão definidos na [biblioteca auxiliar que fizemos](aula04/gabarito/common/README.md), que pode ser importada fazendo:

```lua
-- main.lua
-- Na primeiríssima linha de código do arquivo:
require 'common'
```

Agora o código de mapear controles fica bem simples:

```lua
-- main.lua

function love.load()
  -- Pega dimensões da janela
  local W,H = love.graphics.getDimensions()
  -- Cria o jogador no centro em baixo da janela
  _player = { pos = new(Vec) { x = W/2, y = 9*H/10 } }
end

local CONTROLS = {
  up = new(Vec) { 0, -1 },
  down = new(Vec) { 0, 1 },
  left = new(Vec) { -1, 0 },
  right = new(Vec) { 1, 0 },
}

function love.update(dt)
  -- Controla o jogador
  local movement = new(Vec) {}
  for key,control_movement in pairs(CONTROLS) do
    if love.keyboard.isDown(key) then
      movement = movement + control_movement
    end
  end
  -- Aproveita e normaliza movimento na diagonal!
  _player.pos = _player.pos + movement:normalized() * 300 * dt
end

function love.draw()
  -- ...
  love.graphics.translate(_player.pos:get()) -- devolve x, y
  -- ...
end
```

Para deixar o código mais enxuto, podemos usar `Vec:translate()`:

```lua
-- Note o uso de ':'!
movement:translate(control_movement)
-- ...
_player.pos:translate(movement:normalized() * 300 * dt)
```

## 2. Inimigos

Agora nós queremos que de dois em dois segundos, apareça um inimigo, a princípio inofensivo, na tela. Eles serão quadrados vermelhos, e vão surgir do topo da janela e se movimentar em linha reta para um ponto aleatório na base da tela.

É Ctrl+C, Ctrl+V, só que com 4 diferenças.

### 2.1 Lista de inimigos

+ Sequências em Lua

```lua
-- main.lua
local _enemies
function love.load()
  -- ...
  _enemies = {}
  _enemies[1] = { pos = new(Vec) { W/4, 10 } }
  _enemies[2] = { pos = new(Vec) { W/2, 10 } }
  _enemies[3] = { pos = new(Vec) { 3*W/4, 10 } }
end
```

### 2.2 Visual dos inimigos

+ `for` no `love.draw()`

```lua
-- main.lua
function love.draw()
  -- ...
  for _, enemy in ipairs(_enemies) do
    -- desenhamos aqui!
  end
end
```

+ [`love.graphics.rectangle(mode, x, y, width, height)`](https://love2d.org/wiki/love.graphics.rectangle)

```lua
love.graphics.translate(enemy.pos:get())
love.graphics.setColor(.9, .1, .1)
love.graphics.rectangle('fill', -8, -8, 16, 16)
```

+ `love.graphics.push()` e `love.graphics.pop()`

```lua
love.graphics.push()
-- desenha jogador ou entidade
love.graphics.pop()
```

### 2.3 Criação dos inimigos

+ Timer

```lua
-- main.lua
local _timer
function love.load()
  -- ...
  _timer = 0
end

function love.update(dt)
  _timer = _timer + dt
  if _timer > 2 then
    _timer = _timer - 2
    -- Cria inimigo aqui
  end
  -- ...
end
```
   
+ Posição aleatória

```lua
local W, H = love.graphics.getDimensions()
local new_enemy = {
  pos = new(Vec) { (0.25 + 0.5*love.math.random()) * W, 10 }
}
```

+ `table.insert(table, value)`

```lua
table.insert(_enemies, new_enemy)
```

### 2.4 Movimento dos inimigos

+ Direção fixa, sorteada na criação

```lua
local W, H = love.graphics.getDimensions()
local origin = new(Vec) { (0.25 + 0.5*love.math.random()) * W, 10 }
local target = new(Vec) { (-0.5 + 2*love.math.random()) * W, H + 50 }
local new_enemy = {
  pos = origin,
  movement = (target - origin):normalized()
}
```

+ `for` no `love.update(dt)`

```lua
-- main.lua
function love.update(dt)
  -- ...
  for _, enemy in ipairs(_enemies) do
    enemy.pos:translate(enemy.movement * 200 * dt)
  end
end
```

## 3. Entidades

Jogadores e inimigos são tão parecidos que acabam tendo muito códugo repetido (criação, movimentação, visualização). Podemos juntar os dois em uma mesma categoria de **objetos**. Para isso, vamos criar um novo protótipo de objeto usando a [biblioteca auxiliar](aula04/gabarito/common/README.md).

### 3.0 Interlúdio: objetos por prototipagem

Os códigos nessa subseção são só para explicar como funciona orientação objeto por prototipagem usando a nossa biblioteca auxiliar.

[Para mais informações, leia este artigo sobore o padrão prototype.](http://gameprogrammingpatterns.com/prototype.html)

+ Facilitar criação de vários objetos do mesmo "tipo"

```lua
Vehicle = new(Object) { speed = 10 }
-- Todos veículo agora tem atributo "speed"
car = new(Vehicle) {}
airplane = new(Vehicle) { speed = 100 }
-- Imprime: 10, 100
print(car.speed, airplane.speed)
```

+ Compartilhar métodos

```lua
function Vehicle:move()
  print("moving at " .. self.speed .. " km/h")
end
-- Imprime "moving at 10 km/h"
car:move()
-- Imprime "moving at 100 km/h"
airplane:move()
```

+ Possibilitar herança

```lua
future_car = new(car) {}
-- Se comporta como um carro, a menos de métodos sobrescritos
function future_car:move()
  print("floating at " .. self.speed .. " km/h")
end
-- Imprime: floating at 10 km/h
future_car:move()
-- Imprime: floating at 20 km/h
another_future_car = new(future_car) {
  speed = 20
}
another_future_car:move()
```

+ Construtores

```lua
Hangar = new(Object) {
  stored_vehicles = nil -- não podemos criar lista aqui!
}
-- Construtor cria lista se ela não for fornecida
function Hangar:init()
  self.stored_vehicles = self.stored_vehicles or {}
end
-- Criado com lista pré-definida:
blue_hangar = new(Hangar) {
  stored_vehicles = { new(car) {}, new(airplane) {} }
}
-- Criado com lista vazia
red_hangar = new(Hangar) {}
```

### 3.1 Módulo `entity.lua`

+ Módulos em Lua: cada arquivo Lua é uma função, e pode devolver valores!

```lua
-- entity.lua
local Entity = {}

return Entity
```

```lua
-- main.lua
local Entity = require 'entity'
```

+ Protótipo `Entity`

```lua
-- entity.lua
-- Obs.: o objeto "Object" é criado globalmente pela biblioteca auxiliar
local Entity = new(Object) {
  pos = nil,      -- não podemos criar aqui!
  movement = nil, -- idem
}
function Entity:init()
  self.pos = self.pos or new(Vec) {}
  self.movement = self.movement or new(Vec) {}
end
```

```lua
-- main.lua
_player = new(Entity) { ... }
-- ...
new_enemy = new(Entity) { ... }
```

+ Atalho `new 'entity {}`

Não precisa dar `require` no módulo `entity.lua`!

```lua
-- main.lua
_player = new('entity') { ... }
-- ...
new_enemy = new('entity') { ... }
```

Ou, ainda mais limpo:


```lua
-- main.lua
_player = new 'entity' { ... }
-- ...
new_enemy = new 'entity' { ... }
```

### 3.2 Visualização

+ Método `Entity:draw()`

```lua
-- entity.lua
function Entity:draw()
  love.graphics.push()
  love.graphics.translate(self.pos:get())
  -- Desenha entidade
  love.graphics.pop()
end
```

+ Truque para deixar flexível: campos `color`, `view`, `params`

```lua
-- entity.lua
function Entity:init()
  -- ...
  self.color = self.color or { 1, 1, 1 }
  self.view = self.view or 'rectangle'
  self.params = self.params or { 'fill', -5, -5, 10, 10 }
end

function Entity:draw()
  -- ...
  love.graphics.setColor(self.color)
  love.graphics[self.view](unpack(self.params))
  -- ...
end
```

Precisa colocar os valores respectivos na hora de criar o jogador e os inimigos! A gente transfere do que costumava ficar no `love.draw()`:

```lua
_player = new 'entity' {
  -- ...
  color = { .7, .2, .7 },
  view = 'polygon',
  params = { 'fill',  -10, 5, 10, 5, 0, -15 }
}
-- ...
new_enemy = new 'entity' {
  -- ...
  color = { .9, .1, .1 },
  view = 'rectangle',
  params = { 'fill',  -8, -8, 16, 16 }
}
```

### 3.3 Movimentação

+ Atributo `speed`

```lua
-- entity.lua
local Entity = new(Object) {
  -- ...
  speed = 100
}
```

Precisa arrumar no código que cria o jogador e o inimigo! Você sabe achar qual era a velocidade de cada um deles?

+ Método `Entity:update(dt)`

```lua
-- entity.lua
function Entity:update(dt)
  self.pos:translate(self.movement * self.speed * dt)
end
```

```lua
-- main.lua
_player:update(dt)
-- ...
enemy:update(dt)
```

+ E o `movement` do jogador?

```lua
-- main.lua
function love.update(dt)
  -- ...
  _player.movement:set(movement:get())
  -- ...
end
```

### 3.4 Generalização

+ Juntar tudo em uma sequência `_entities`

Agora, o jogador também é uma entidade, e todas são movidas e desenhadas de maneira indiscriminada.

```lua
-- main.lua
local _entities
function love.load()
  _entities = {}
  -- ...
  table.insert(_entities, _player)
end
```

O que mais precisa mudar? Você lembra? Na dúvida, rode o jogo e veja o que acontece. Se der erro, leia a mensagem. Provavelmente vão haver referências à variável `_enemies` que não existe mais; nesse caso é só trocar por `_entities`.

+ Fazer uma função criadora de entidades `_makeEntity(typename, pos, movement)`

```lua
-- main.lua
local function _makeEntity(typename, pos, movement)
  local new_entity
  if typename == 'player' then
    new_entity = new 'entity' { ... }
  elseif typename == 'enemy' then
    new_entity = new 'entity' { ... }
  else
    return error("Unknown entity type")
  end
  new_entity.pos:set(pos:get())
  if movement then
    new_entity.movement = movement:normalized()
  end
  table.insert(_entities, new_entity)
  return new_entity
end
```

Agora fica mais fácil de criar entidades:

```lua
_player = _makeEntity('player', pos, nil) -- podia omitor o último parâmetro
-- Na hora de criar inimigos:
_makeEntity('enemy', origin, target - origin)
```

## Continuação

Os passos 4 em diante do exercício foram alocados para [a quinta aula](aula05)!