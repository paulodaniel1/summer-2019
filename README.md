# Programação para Jogos Digitais

<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Licença Creative Commons" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a>

Material usado na edição do curso de verão em Janeiro-Fevereiro de 2019.

## Organização do curso

O curso tem **6 semanas de aulas**, cada uma com duas aulas de 4h, das 14h às 18h.

Nas duas primeiras semanas, vamos ensinar toda a teoria básica acompanhada de exercícios.

Nas outras quatro semanas, os alunos vão trabalhar nos projetos, mas vamos passar mais alguns conceitos e técnicas também

As aulas são divididas em dois blocos de 1h40: 14-15h40 e 16h-17h40. A margem de tempo é tanto para nos planejarmos para atrasos, quanto para ter um break mesmo entre os dois blocos.

As partes expositivas sempre ficam no primeiro bloco. Quando não houver parte expositiva numa aula, os dois blocos são usados para prática.

## Cronograma

### Primeira semana

Na primeira semana vamos dar uma noção geral da infraestrutura mínima que um jogo precisa.

#### Aula 01 - 7/1/2019 - the omega and the alfa

Onde jogos começam e acabam, e como o curso vai ensinar isso.

+ Apresentação do curso
+ Linha de produção de jogos
+ Game Loop
+ Introdução à LÖVE

#### Aula 02 - 10/1/2019 - interaction

Como funciona a parte interativa de jogos: entrada e saída, em particular na LÖVE

+ Dispositivos de Interface Humana (HID)
+ Renderização gráfica
+ Renderização sonora

#### Aula 03 - 14/1/2019 - game it on 1

Primeira parte sobre simulação em jogos

+ Mecânicas
+ Modelo de objetos
+ Estado do jogo

#### Aula 04 - 17/1/2019 - game it on 2

Segunda parte sobre simulação em jogos

+ Revisão aula 3
+ Projeto de Software: código repetido
  + Orientação a objetos e herança
  + Data-driven design

#### Aula 05 - 21/1/2019 - step it up

Primeira parte sobre organização e projeto de código em jogos

+ Projeto de software: coesão
  + Modularização
  + Padrão de projeto: State
+ Projeto de Software: acoplamento
  + Eventos e mensagens
  + Requisições

#### Aula 06 - 24/1/2019 - it's dangerous to go alone, take this! 1

Técnicas avançadas de desenvolvimento

+ Motores de jogos
+ Arquitetura de motores de jogos
  + Padrão arquitetural: Camadas
  + Padrão arquitetural: Model-View-Controller
+ Ferramentas de desenvolvimento e depuração

#### Aula 07 - 28/1/2019 - projeto 1, parte 1

Aula dedicada a trabalho no projeto

#### Aula 08 - 31/1/2019 - projeto 1, parte 2

Aula dedicada a trabalho no projeto

#### Aula 09 - 4/2/2019 - projeto 1, parte 3

Aula dedicada a trabalho no projeto

#### Aula 10 - 7/2/2019 - projeto 2, parte 1

Aula dedicada a trabalho no projeto

#### Aula 11 - 11/2/2019 - projeto 2, parte 2

Aula dedicada a trabalho no projeto

#### Aula 12 - 14/2/2019 - projeto 2, parte 3

Aula dedicada a trabalho no projeto