# Aula 03 (10/1/2019) - *game it on, part ONE*

Primeira parte sobre simulação em jogos

## Mecânicas [20 min]

São as regras que ditam a narrativa do jogo, as relaçoes de causa e feito dentro
do mundo fictício, e as possibilidades de intervenção à disposição dos
jogadores.

É através das mecânicas que a experiência do jogo é construída, e esse é o
objeto de trabalho dos game designers.

Podemos classificar mecânicas em alguns tipos:

1. Mecãnicas de espaço e movimento (física)
2. Mecânicas de progressão narrativa
3. Mecânicas de economia interna
4. Mecânicas táticas
5. Mecânicas de interação social

## Objetos

Uma prática comum em jogos é encarar as entidades do jogo através do paradigma
de **programação orientada a objetos** (POO).

### Breve revisão de POO [10 min]

Objetos são o acoplamento entre uma porção de dados e de funções. As funções
operam dentro do contexto dos dados do objeto, e qualquer ação sobre dados
externos envolve usar outros objetos.

### Modelo do objeto [40 min]

A especificação de todos os objetos que podem existir em um jogo formam o
**modelo de objetos** (*object model*). Esse modelo diz quais tipos de objetos
existem, quais características eles podem ter e quais operações ele podem
executar. Podemos dizer que ele define um vocabulário de substantivos,
adjetivos e verbos com o qual o jogo expressa sua narrativa.

A maneira mais direta de implementar um modelo de objetos é fazer que o tipo de
um objeto é dado pela sua própria classe. Mas existem outras práticas, cada uma
com suas vantagens e desvantagens. Essas técnicas não são mutuamente exclusivas.

#### Tipos são classes

O modelo é definido pela própria hierarquia de classes de POO. O modelo é
definido em tempo de compilação.

#### Tipos são objetos (Adaptive Object-Model)

O tipo de um objeto é outro objeto, chamado de
[Type Object](http://gameprogrammingpatterns.com/type-object.html). O Type
Object pode ter seus atributos definidos em tempo de execução, por exemplo,
carregando de um arquivo. Isso permite que o modelo (isto é, as especificações
dos objetos no jogo) mude em tempo de execução.

É possível ter hierarquia de tipos combinando essa técnica com o padrão de
projeto [Prototype](http://gameprogrammingpatterns.com/prototype.html).

#### Tipos são composições (Entity-Component System)

Os dados e o comportamento de um objeto são definidos por vários outros objetos
que ele possui, chamados de
[componentes](http://gameprogrammingpatterns.com/component.html). O objeto em
si pode ser inclusive vazio, virando apenas um contâiner de seus componentes.
Além disso, os componentes podem ser removidos e adicionados durante o jogo,
mudando parcialmente as características do objeto em tempo de execução.

Nesta técnica a hierarquia não é clara, mas em compensação há maior
flexibilidade de combinação de especificações.

### Estado do jogo [30 min]

O estado dos objetos em um jogos muda constantemente, e essas mudanças seguem
as regras impostas pelas mecânicas. Exemplos de mudanças no estado:

1. Criação e destruição
2. Operações nos atributos do objeto (alterações quantitativas)
3. Mudanças de comportamento (alterações qualitativas): podem usar o padrão de projeto [State](http://gameprogrammingpatterns.com/state.html) ou [Decorator](https://en.wikipedia.org/wiki/Decorator_pattern)

## Exercício [100 min]

1. Creation
2. AI
3. Shooting
4. Collision
5. Damage
6. Death

