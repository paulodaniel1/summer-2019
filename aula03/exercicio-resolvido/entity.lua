
local Entity = new(Object) {
  spec = nil,
  cooldown = 0
}

function Entity.load(name)
  return new(Entity) {
    spec = new 'spec' (require('database.' .. name))
  }
end

function Entity:init()
  self.pos = self.pos or new(Vec) { 0, 0 }
  self.dir = new(Vec) { 0, 0 }
end

function Entity:setDirection(dir)
  self.dir = dir:normalized()
end

function Entity:update(entities, dt)
  self.pos:translate(self.dir * self.spec.speed * dt)
  self.cooldown = self.cooldown + dt
  while self.cooldown > 1 / self.spec.firerate do
    self.cooldown = self.cooldown - 1 / self.spec.firerate
    local new_entity = Entity.load(self.spec.bullet)
    new_entity.pos:set(self.pos:get())
    new_entity:setDirection(new(Vec) { 0, -1 })
    table.insert(entities, new_entity)
  end
end

function Entity:draw()
  local view = self.spec.view
  local g = love.graphics
  g.push()
  g.translate(self.pos:get())
  g.setColor(view.color)
  g[view.type](unpack(view.params))
  g.pop()
end

return Entity

