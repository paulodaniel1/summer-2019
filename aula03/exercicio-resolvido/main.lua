
require 'common'

local Entity = require 'entity'

local _entities
local _ais
local _player
local _entity_timer

function love.load()
  local W, H = love.graphics.getDimensions()
  _entities = {}
  _ais = {}
  _player = Entity.load 'player'
  _player.pos:set(W/2, 9*H/10)
  table.insert(_entities, _player)
  _entity_timer = 0
end

local CONTROLS = {
  up = new(Vec) { 0, -1 },
  down = new(Vec) { 0, 1 },
  left = new(Vec) { -1, 0 },
  right = new(Vec) { 1, 0 },
}

function love.update(dt)
  -- Update entity timer
  _entity_timer = _entity_timer + dt
  if _entity_timer >= 2 then
    -- Reseta contador
    _entity_timer = 0
    -- Cria entidade nova
    local W = love.graphics.getDimensions()
    local new_entity = Entity.load 'player'
    new_entity.pos:set(love.math.random() * W, 10)
    table.insert(_entities, new_entity)
    -- Defina IA da entidade
    local ai = new 'ai' {}
    ai:setEntity(new_entity)
    table.insert(_ais, ai)
  end
  -- AI control
  for _,ai in ipairs(_ais) do
    ai:think()
  end
  -- Player control
  local dir = new(Vec) {}
  for key,control_dir in pairs(CONTROLS) do
    if love.keyboard.isDown(key) then
      dir:translate(control_dir)
    end
  end
  _player:setDirection(dir)
  -- Update entities
  for _,entity in ipairs(_entities) do
    entity:update(_entities, dt)
  end
end

function love.draw()
  for _,entity in ipairs(_entities) do
    entity:draw()
  end
end

