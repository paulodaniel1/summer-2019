
require 'common'

local Entity = require 'entity'

local _entities
local _player

function love.load()
  local W, H = love.graphics.getDimensions()
  _entities = {}
  _player = Entity.load 'player'
  _player.pos:set(W/2, 9*H/10)
  table.insert(_entities, _player)
end

local CONTROLS = {
  up = new(Vec) { 0, -1 },
  down = new(Vec) { 0, 1 },
  left = new(Vec) { -1, 0 },
  right = new(Vec) { 1, 0 },
}

function love.update(dt)
  -- Player control
  local dir = new(Vec) {}
  for key,control_dir in pairs(CONTROLS) do
    if love.keyboard.isDown(key) then
      dir:translate(control_dir)
    end
  end
  _player:setDirection(dir)
  -- Update entities
  for _,entity in ipairs(_entities) do
    entity:update(dt)
  end
end

function love.draw()
  for _,entity in ipairs(_entities) do
    entity:draw()
  end
end

