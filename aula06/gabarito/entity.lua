
local Entity = new(Object) {
  spec = nil,
  world = nil,
  pos = nil,
  movement = nil,
  dir = 0,
  cooldown = 0
}

function Entity:init()
  assert(self.spec and self.world)
  self.pos = self.pos or new(Vec) { 0, 0 }
  self.movement = new(Vec) { 0, 0 }
end

function Entity:setMovement(movement)
  self.movement = movement:normalized()
end

function Entity:update(dt)
  self.pos:translate(self.movement * self.spec.speed * dt)
  self.cooldown = self.cooldown + dt
  while self.cooldown > 1 / self.spec.firerate do
    self.cooldown = self.cooldown - 1 / self.spec.firerate
    local dirvec = Vec.fromAngle(self.dir)
    local pos = self.pos + dirvec * 20
    self.world:makeEntity(self.spec.bullet_specname, pos, self.dir, dirvec)
  end
end

function Entity:collidesWith(other)
  return (self.spec.hitbox + self.pos):intersects(other.spec.hitbox + other.pos)
end

function Entity:draw()
  local view = self.spec.view
  local g = love.graphics
  g.push()
  g.translate(self.pos:get())
  g.setColor(view.color)
  g[view.type](unpack(view.params))
  g.pop()
end

return Entity

