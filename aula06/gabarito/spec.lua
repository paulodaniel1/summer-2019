
local Spec = new(Object) {
  name = "Undefined",
  speed = 100,
  firerate = 0,
  bullet_specname = nil,
  hitbox = { -5, 5, -5, 5 },
  view = {
    color = { 1, 1, 1 },
    type = 'rectangle',
    params = { 'fill', -5, -5, 10, 10 }
  }
}

function Spec:init()
  self.hitbox = new(Box) (self.hitbox)
end

return Spec

