
# Aula 6

Nessa aula, vamos concluir o exercício das aulas anteriores. Estamos desenvolvendo um jogo 2D shooter bem simples, que
pode ser visto e testado na pasta `gabarito` desta aula.

### Instruções

Para fazer os passos dessa aula **VOCÊ PRECISA TER FEITO *TODOS* OS PASSOS DA QUARTA AULA E DA QUINTA AULA**.

Além disso, lembre-se de **nunca pular passos**. A sequência do exercício foi projetada para construir novas mecânicas em cima das mudanças anteriores, então as instruções ficam completamente sem sentido se seu código não tiver seguido todos os passos até aquele ponto.

Uma recomendação para ajudar a assimilar a sintaxe e o quê cada passo significa: **escrevam vocês mesmos o código indicado nos passos, não copiem!**

Por fim, tente entender o que cada passo está fazendo:

+ Por que estamos fazendo esse passo? Qual o objetivo dele?
+ O que esse trecho de código faz? Como ele contribui para o objetivo?
+ Como esse passo se encaixa no jogo como um todo? Onde eu coloco ele?

### Conceitos que vamos trabalhar

Na última aula nós falamos sobre **coesão** e **acoplamento**. Nesta aula, nós vamos nos concentrar em terminar e entender o exercício.

### Índice

[7. Tiros](#7-tiros-agora-sim)

[8. Mais entidades!](#8-mais-entidades)

## 7. Tiros (agora sim)

Toda entidade agora vai poder disparar outras entidades ("projéteis").

### 7.1 Quando ocorre um disparo?

+ Taxa de disparo (*firerate*)

Toda entidade precisa definir uma *taxa de disparo*, que chamamos de `firerate` e é medido em "projéteis disparados por segundo":

```lua
-- entity.lua
local Entity = new(Object) {
  -- ...
  firerate = 0
}
```

No caso, apenas o jogador vai disparar projéteis, então mudamos sua criação em `World:makeEntity(...)`:

```lua
new_entity = new 'entity' {
  -- ...
  firerate = 10
}
```

+ Cooldown

No entanto, para de fato saber *quando* uma entidade dispara algo, precisamos considerar quanto tempo leva entre cada disparo. Isso é o inverso da taxa de disparo! Para contar o tempo entre disparos, fazemos um timer em cada entidade.

```lua
-- entity.lua
local Entity = new(Object) {
  -- ...
  shooting_timer = 0
}
```

E acrescentamos um código no `Entity:update(dt)` para verificar se o tempo certo passou:

```lua
self.shooting_timer = self.shooting_timer + dt
if self.shooting_timer > 1 / self.firerate then
  self.shooting_timer = self.shooting_timer - 1 / self.firerate
  -- Dispara projétil!
end
```

Note, no entanto, que o tempo entre disparos pode ser bem curto, inclusive menor que `dt`! Pense, por exemplo, em uma entidade com `firerate` 100 (lembrando que o jogo roda a 60 fps). Isso significa que mesmo depois de subtrair `1 / self.firerate`, pode ser que ainda tenha tempo o bastante acumulado em `self.shootingtimer` para outro disparo. Portanto, continuarmos verificando:

```lua
while self.shooting_timer > 1 / self.firerate do
  -- ...
end
```

### 7.2 Criação de projéteis

+ Mudar a função criadora para aceitar o tipo `bullet`

Agora nós queremos criar um terceiro tipo de entidade, que chamaremos de `bullet`. Se formos seguir o padrão anterior, acrescentaríamos um `if` em `World:makeEntity(...)`, mas nesse ponto já dá pra ver que isso não é escalável. De qualquer forma, vamos fazer assim mesmo primeiro e refatorar logo em seguida:

```lua
elseif typename == 'bullet' then
  new_entity = new 'entity' {
    speed = 600,
    hitbox = new(Box) { -4, 4, -4, 4 },
    color = { 1, 1, 1 },
    view = 'rectangle',
    params = { 'fill',  -4, -4, 8, 8 }
  }
end
```

+ Criar projétil

```lua
-- entity.lua
local dirvec = new(Vec) { 0, -1 } -- para cima
local pos = self.pos + dirvec * 20
self.world:makeEntity('bullet', pos, dirvec)
```

### 7.3 Direção dos projéteis

Do jeito que está, toda entidade atira pra cima, mas e se quisermos inimigos que atiram pra baixo?

+ Fazer atributo `Entity.dir`

```lua
-- entity.lua
local Entity = new(Object) {
  -- ...
  dir = 0, -- ângulo em graus!
}
```

+ Usar na criação dos projéteis

```lua
-- entity.lua
local dirvec = Vec.fromAngle(self.dir)
local pos = self.pos + dirvec * 20
self.world:makeEntity('bullet', pos, dirvec)
```

+ Acrescentar em `World:makeEntity(typename, pos, movement, dir)`

```lua
-- world.lua
function World:makeEntity(typename, pos, movement, dir)
  -- ...
  new_entity.dir = dir or 0
end
```

Assim dá para fazer instâncias do mesmo tipo de entidade dispararem em direções diferentes.

Em particular, você precisa arrumar o código que cria o jogador para passar o ângulo certo de disparo, que vai ser 270 graus (para cima).


### 7.4 (Opcional) Refatoração do `World:makeEntity(...)`

+ Usar uma tabela ao invés de `if`s

Ao invés de usar `if`s, podemos fazer um dicionário onde cada nome de tipo (`player`, `enemy` e `bullet`) está associado à tabela com os valores de atributos para aquele tipo:

```lua
-- world.lua
local ENTITY_DATABASE = {
  player = { ... },
  enemy = { ... },
  bullet = { ... }
}
-- ...
function World:makeEntity(typename, pos, movement)
  local type_data = ENTITY_DATABASE[typename]
  local new_entity = new 'entity' {
    speed = type_data.speed,
    firerate = type_data.firerate,
    -- e assim por diante
  }
  -- ...
end
```

Mas nós podemos fazer melhor!

+ Type Object

Por que copiar os atributos, se eles nunca mudam e vão ficar ocupando espaço atoa nas centenas de entidades do nosso jogo? Nós podemos simplesmente fazer as entidades terem uma referência para o tipo delas. Vamos chamar esse atributo de `spec` de "specification" (em parte porque `type` é uma palavra meio reservada em Lua):

```lua
local new_entity = new 'entity' {
  spec = ENTITY_DATABASE[typename],
  -- ...
}
```

Obviamente, isso requer diversas mudanças no código para acessar os campos que antes ficavam direto na entidade e agora ficam na especificação dela. Por exemplo, o código de movimento agora fica:

```lua
self.pos:translate(self.movement * self.spec.speed * dt)
```

O resto fica a cargo do leitor =)

+ Fazendo algo mais parecido com um banco de dados de verdade

Crie uma pasta `database` e faça arquivos `player.lua`, `enemy.lua` e `bullet.lua` que devolvel, respectivamente, as especificações de cada um desses tipos de entidade. Depois, ajuste o código em `World:makeEntity(...)`:

```lua
local new_entity = new 'entity' {
  spec = require('database.' .. typename) -- afinal, "require" é uma função como qualquer outra,
  -- ...
}
```

## 8. Mais entidades!

Agora deixe sua imaginação soltar e criar vários tipos de entidades! Você vai ter que pensar em duas questões:

1. Como mudar o tipo de projétil de uma entidade?
2. Como fazer o mundo criar outros tipos de entidade?

Outras sugestões de mecânicas:

1. Entidades podem disparar mais de um projétil por vez (atributo `burst`)
2. Entidades podem disparar projéteis em um ângulo (atributo `spread`)
3. O tempo entre a criação de inimigos diminui aos poucos, fazendo aparecem inimigos cada vez mais rápido
4. Entidades possuem pontos de vida e poder de ataque, e a colisão apenas faz elas trocarem dano
5. Supondo a mecânica acima, como evitar duas entidades colidindo tomem dano todo frame? Como outros jogos fazem isso?

E a resposta para essas duas coisas é **Data-Driven Design**.

## Experimento

+ [Formulário de perfil do participante](https://goo.gl/forms/iAKpHYBSBMgghP0O2)


